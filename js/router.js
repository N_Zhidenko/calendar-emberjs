Calendar.Router.map(function(){
  this.resource('calendar',  {path: '/'}, function() {
    this.resource('year', {path: 'year/:year'}, function(){
      this.resource('month', {path: 'month/:month'}, function(){
        this.resource('day', { path: 'day/:day'});
      });
    });
  });
});


Calendar.CalendarIndexRoute = Ember.Route.extend({
  model: function() {
    var model = { };
    date = new Date();
    model.day = date.getDate();
    model.month = date.getMonth() + 1;
    model.year = date.getFullYear();    
    this.transitionTo('day', model.year, model.month, model.day);
  }
});

Calendar.YearRoute = Ember.Route.extend({
  model: function(params) {
    var model = this.get('model');
    model.year = params.year;
    return model;
  }
});

Calendar.MonthRoute = Ember.Route.extend({
  model: function(params) {
    var model = this.get('model');
    model.month = params.month;
    return model;
  }
});


Calendar.DayRoute = Ember.Route.extend({
  model: function(params) {
    var model = this.get('model');
    model.day = params.day;
    model.month = this.modelFor('month').month;
    model.year = this.modelFor('year').year;
    this.transitionTo('day', model.year, model.month, model.day);
    return model;
  }
});