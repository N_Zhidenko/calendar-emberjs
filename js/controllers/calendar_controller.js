Calendar.DayController = Ember.ObjectController.extend({
  transition: function(){
    this.transitionToRoute('day', this.get('year'), this.get('month'), this.get('day'));
  },
  
  check: function(){
    if(this.month > 12)
    {
      this.set('year', parseInt(this.get('year')) + 1);
      this.set('month', 1);
    } 
    else if(this.month < 1)
    {
      this.set('year', this.get('year') - 1);
      this.set('month', 12);
    }
    
    var date = new Date(this.get('year'), this.get('month'), 0);
    if(date.getDate() < parseInt(this.get('day')))
    {
      this.set('day', date.getDate());
    }
  },
  
  setDate: function(date){
    this.set('year', date.getFullYear());
    this.set('month', date.getMonth() + 1);
    this.set('day', date.getDate());
  },
  
  actions: {
    prevYear: function() {
      this.set('year', this.get('year') - 1);
      this.check();
      this.transition();
    },
    nextYear: function() {
      this.set('year', parseInt(this.get('year')) + 1);
      this.check();
      this.transition();
    },
    prevMonth: function() {
      this.set('month', this.get('month') - 1);
      this.check();
      this.transition();
    },
    nextMonth: function() {
      this.set('month', parseInt(this.get('month')) + 1);
      this.check();
      this.transition();
    },
    toDay: function() {
      this.setDate(new Date());
      this.transition();
    },
    select: function(day) {      
      if(day.isDay)
        this.set('day', day.title);
      this.transition();
    }
  },
  
  year: function()
  {
    var model = this.get('model');
    return model.year;
  }.property('year'),
  
  month: function()
  {
    var model = this.get('model');
    return model.month == 0? "Monday" : model.month;
  }.property('month'),
                                                       
  day: function()
  {
    return this.get('model').day;
    
  }.property('day'),
                                                      
  weeks: function () {
  
    var maxDay = new Date(this.get('year'), this.get('month'), 0).getDate(),
        toDay = new Date();
    var firstDayOfWeek = new Date(this.get('year'), this.get('month') - 1, 1).getDay() - 1;
  
    var day = 1,
        countOfWeeks = 6;
  
    var arrayOfWeeks = [],
        tempWeek = [];
    
    for(var i = 0; i < countOfWeeks && maxDay >= day; i++)
    {
      for(var j = 0; j < 7; j++)
      {
        var check = j < firstDayOfWeek || maxDay < day;
        tempWeek.push(Ember.Object.create({
          title: check ? "" : day++,
          select: false,
          isDay: check ? false : true,
          isActive: parseInt(this.get('day')) == parseInt(day - 1) && !check ? true : false,
          isToday: false
        }));
      }
      
      arrayOfWeeks.push(tempWeek);
      tempWeek = [];
      
      
      firstDayOfWeek = -1;
    }
    
    if(parseInt(this.get('month')) == (toDay.getMonth() + 1) && 
       parseInt(this.get('year')) == (toDay.getFullYear()))
       {
        firstDayOfWeek = new Date(this.get('year'), this.get('month') - 1, 1).getDay() - 1;
        var date = toDay.getDate() + firstDayOfWeek;
        arrayOfWeeks[parseInt(date/7)][parseInt(date%7) - 1].set('isToday', true);
       }
       
    
    
    return arrayOfWeeks;
  }.property('year', 'month', 'day')
                                                       
});
